package com.rpo.backend.controllers;

import com.rpo.backend.models.Actor;
import com.rpo.backend.models.Country;
import com.rpo.backend.repositories.ActorRepository;
import com.rpo.backend.repositories.CountryRepository;
import com.rpo.backend.tools.DataValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/v1")
public class ActorController {
    @Autowired
    ActorRepository actorRepository;
    @Autowired
    CountryRepository countryRepository;

    // GET запрос, метод возвращает список актёров, который будет автоматически преобразован в JSON
    @GetMapping("/actors")
    public Page<Actor> getAllActors(@RequestParam("page") int page, @RequestParam("limit") int limit) {
        return actorRepository.findAll(PageRequest.of(page, limit, Sort.by(Sort.Direction.ASC, "name")));
    }

    @GetMapping("/actors/{id}")
    public ResponseEntity<Actor> getActor(@PathVariable(value = "id") Long actorId)
            throws DataValidationException
    {
        Actor actor = actorRepository.findById(actorId)
                .orElseThrow(()-> new DataValidationException("Актёр с таким индексом не найден"));
        return ResponseEntity.ok(actor);
    }

    // POST запрос, сохраняем в таблице actors нового актёра
    // В теле метода проверяем, что существует country с заданным id,
    // Если да, то actor.country = <Название_страны>
    // Без проверки было бы null
    @PostMapping("/actors")
    public ResponseEntity<Object> createActor(@Valid @RequestBody Actor actor) {
        Optional<Country> cc = countryRepository.findById(actor.country.id);
        if(cc.isPresent()) {
            actor.country = cc.get();
        }
        Actor nc = actorRepository.save(actor);
        return new ResponseEntity<Object>(nc, HttpStatus.OK);
    }

    // PUT запрос, обновляет запись в таблице actors
    // Здесь обработка ошибки сводится к тому, что мы возвращаем код 404 на запрос в случае,
    // если актёр с указанным ключом отсутствует
    @PutMapping("/actors/{id}")
    public ResponseEntity<Actor> updateActor(@PathVariable(value = "id") Long actorId,
                                               @Valid @RequestBody Actor actorDetails) {
        Actor actor = null;
        Optional<Actor> aa = actorRepository.findById(actorId);
        if (aa.isPresent())
        {
            actor = aa.get();
            actor.name = actorDetails.name;
            actor.century = actorDetails.century;
            actor.country = actorDetails.country;
            actorRepository.save(actor);
        }
        else
        {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "actor_not_found"
            );
        }
        return ResponseEntity.ok(actor);
    }

    // Метод удаления записи из таблицы actors
    @PostMapping("/deleteactors")
    public ResponseEntity deleteActors(@Valid @RequestBody List<Actor> actors) {
        actorRepository.deleteAll(actors);
        return new ResponseEntity(HttpStatus.OK);
    }
}
