package com.rpo.backend.controllers;

import com.rpo.backend.models.Film;
import com.rpo.backend.repositories.CountryRepository;
import com.rpo.backend.repositories.FilmRepository;
import com.rpo.backend.tools.DataValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/v1")
public class FilmController {
    @Autowired
    FilmRepository filmRepository;

    @Autowired
    CountryRepository countryRepository;

    @GetMapping("/films")
    public Page<Film> getAllFilms(@RequestParam("page") int page, @RequestParam("limit") int limit) {
        return filmRepository.findAll(PageRequest.of(page, limit, Sort.by(Sort.Direction.ASC, "name")));
    }

    @GetMapping("/films/{id}")
    public ResponseEntity<Film> getPainting(@PathVariable(value = "id") Long filmId)
            throws DataValidationException
    {
        Film painting = filmRepository.findById(filmId)
                .orElseThrow(()-> new DataValidationException("Фильм с таким индексом не найдена"));
        return ResponseEntity.ok(painting);
    }

    @PostMapping("/films")
    public ResponseEntity<Object> createFilm(@Valid @RequestBody Film film) {
        Film nc = filmRepository.save(film);
        return new ResponseEntity<Object>(nc, HttpStatus.OK);
    }

    @PutMapping("/films/{id}")
    public ResponseEntity<Film> updateFilm(@PathVariable(value = "id") Long filmId,
                                                   @Valid @RequestBody Film filmDetails) {
        Film film = null;
        Optional<Film> mm = filmRepository.findById(filmId);
        if (mm.isPresent())
        {
            film = mm.get();
            film.name = filmDetails.name;
            film.year = filmDetails.year;
            filmRepository.save(film);
        }
        else
        {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "film_not_found"
            );
        }
        return ResponseEntity.ok(film);
    }

    @PostMapping("/deletefilms")
    public ResponseEntity deleteActors(@Valid @RequestBody List<Film> films) {
        filmRepository.deleteAll(films);
        return new ResponseEntity(HttpStatus.OK);
    }
}
